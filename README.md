# README #

Just a very small python program to convert XML from a file into plain text (full path and value on each line) and write it to a second file.


```
#!bash

usage: XML-to-CSV-Converter v0.1.0 [-h] xml csv

positional arguments:
  xml         Input XML file name
  csv         Output CSV file name

optional arguments:
  -h, --help  show this help message and exit

```