'''
Created on 26.02.2017

@author: chris
'''

from argparse import ArgumentParser
from lxml import etree


__myname__ = 'xml-to-csv'
__myshortname__ = 'xml2csv'
__title__ = 'XML-to-CSV-Converter'
__version__ = '0.1.0'


def main():
    args = parse_command_line()

    with open(args.xml, 'r') as f:
        with open(args.csv, 'w') as output:
            tree = etree.parse(f)
            root = tree.getroot()
            i = 0
            for element in root.iter("*"):
                try:
                    text = '"' + element.text.strip() + '"'
                except:
                    text = '<NOTHING>'

                path = '{}/{} = {}'.format(tree.getpath(element),
                                           element.tag, text)
                output.write(path + '\n')
                i += 1

            print('{} elements written'.format(i))


def parse_command_line():
    '''
    Parse the command line parameters
    '''
    parser = ArgumentParser(
        prog=__title__ + ' v' + __version__, description=main.__doc__)

    parser.add_argument('xml', help='Input XML file name')
    parser.add_argument('csv', help='Output CSV file name')

    args = parser.parse_args()
    return(args)

if __name__ == '__main__':
    main()
